// @flow

import React, { Component } from 'react';
import { Platform } from 'react-native';

import { ColorSchemeRegistry } from '../../../base/color-scheme';
import { BottomSheet, hideDialog, isDialogOpen } from '../../../base/dialog';
import { CHAT_ENABLED, IOS_RECORDING_ENABLED, getFeatureFlag } from '../../../base/flags';
import { connect } from '../../../base/redux';
import { StyleType } from '../../../base/styles';
import { getLocalParticipant } from '../../../base/participants';
import { InviteButton } from '../../../invite';

/**
 * The type of the React {@code Component} props of {@link OverflowMenu}.
 */
type Props = {

    /**
     * The color-schemed stylesheet of the dialog feature.
     */
    _bottomSheetStyles: StyleType,

    /**
     * Whether the chat feature has been enabled. The meeting info button will be displayed in its place when disabled.
     */
    _chatEnabled: boolean,

    /**
     * True if the overflow menu is currently visible, false otherwise.
     */
    _isOpen: boolean,

    /**
     * Whether the recoding button should be enabled or not.
     */
    _recordingEnabled: boolean,

    /**
     * Used for hiding the dialog when the selection was completed.
     */
    dispatch: Function,


    _participant: any,
    _participantsInCall: any
};

/**
 * The exported React {@code Component}. We need it to execute
 * {@link hideDialog}.
 *
 * XXX It does not break our coding style rule to not utilize globals for state,
 * because it is merely another name for {@code export}'s {@code default}.
 */
let OverflowMenu_; // eslint-disable-line prefer-const

/**
 * Implements a React {@code Component} with some extra actions in addition to
 * those in the toolbar.
 */
class OverflowMenu extends Component<Props> {
    /**
     * Initializes a new {@code OverflowMenu} instance.
     *
     * @inheritdoc
     */
    constructor(props: Props) {
        super(props);

        this.fetchedUsersInCall = false;
        this.participantsInCall = [];

        _getCallParticipants(this.props._participant).then(object => {
            if (object !== null) {
                if (object.result.numUsers >= 10) {
                    this._onCancel();

                    this.fetchedUsersInCall = true;
                    this.participantsInCall = object.result.users;


                }
            }
        });


        // Bind event handlers so they are only bound once per instance.
        this._onCancel = this._onCancel.bind(this);
    }

    /**
     * Initializes a new {@code OverflowMenu} instance.
     *
     * @inheritdoc
     */
    renderButtons() {
        const buttonProps = {
            afterClick: this._onCancel,
            showLabel: true,
            styles: this.props._bottomSheetStyles,
            userName: 'item.name',
            contactIsAvailable: 'item.status',
            jid: 'item.jid',
            hasAvatar: 'item.avatar',
            participant: this.props._participant,
            inCall: false
        };


        return (
            <InviteButton
                key = { 'item.jid' }
                { ...buttonProps } />
        );
    }

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {

        return (
            <BottomSheet onCancel = { this._onCancel } />
        );
    }

    _onCancel: () => boolean;

    /**
     * Hides this {@code OverflowMenu}.
     *
     * @private
     * @returns {boolean}
     */
    _onCancel() {
        if (this.props._isOpen) {
            this.props.dispatch(hideDialog(OverflowMenu_));

            return true;
        }

        return false;
    }
}

/**
 * Function that maps parts of Redux state tree into component props.
 *
 * @param {Object} state - Redux state.
 * @private
 * @returns {Props}
 */
function _mapStateToProps(state) {
    return {
        _bottomSheetStyles:
            ColorSchemeRegistry.get(state, 'BottomSheet'),
        _chatEnabled: getFeatureFlag(state, CHAT_ENABLED, true),
        _isOpen: isDialogOpen(state, OverflowMenu_),
        _recordingEnabled: Platform.OS !== 'ios' || getFeatureFlag(state, IOS_RECORDING_ENABLED),

        _participant: getLocalParticipant(state)
    };
}

/**
 * Requests users in call {@code OverflowMenu}.
 *
 * @private
 * @param {Object} participant - Well the participant.
 * @returns {Object} Json -  Well... The response.
 */
async function _getCallParticipants(participant) {
    const url = `${participant.serviceURL}`;

    const form = new FormData();

    form.append('action', 'usersincall');
    form.append('idCall', participant.idCall);

    const response = await fetch(url, { method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        body: form
    });

    const json = await response.json();

    return json;
}


OverflowMenu_ = connect(_mapStateToProps)(OverflowMenu);

export default OverflowMenu_;
