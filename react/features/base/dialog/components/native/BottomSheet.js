// @flow

import React, { PureComponent, type Node } from 'react';
import {
    View,
    FlatList,
    SafeAreaView,
    ActivityIndicator,
    TextInput
} from 'react-native';
import { BackButton, Header, HeaderLabel, Modal } from '../../../react';
import { DARK_GREY } from '../../../../invite/components/add-people-dialog/native/styles';
import { InviteButton } from '../../../../invite';

import { ColorSchemeRegistry } from '../../../color-scheme';
import { connect } from '../../../redux';
import { StyleType } from '../../../styles';


import { getLocalParticipant } from '../../../participants';
import { Icon } from '../../../icons/components';
import { IconSearch } from '../../../icons/svg';

/**
 * The type of {@code BottomSheet}'s React {@code Component} prop types.
 */
type Props = {

    /**
     * The color-schemed stylesheet of the feature.
     */
    _styles: StyleType,

    /**
     * The children to be displayed within this component.
     */
    children: Node,

    /**
     * Handler for the cancel event, which happens when the user dismisses
     * the sheet.
     */
    onCancel: ?Function,

    _participant: any,
    page: number
};

/**
 * A component emulating Android's BottomSheet.
 */
class BottomSheet extends PureComponent<Props> {

    state = {
        data: [],
        search: '',
        page: 1,
        loading: true,
        noContacts: false,
        totalContacts: -1,
        usersInCall: 0
    }

    /**
     * Initializes a new {@code BottomSheet} instance.
     *
     * @inheritdoc
     */
    constructor(props: Props) {
        super(props);

        this._onCancel = this._onCancel.bind(this);
    }

    componentDidMount() {
        this._fetchMoreContacts()
            .catch(error => {
                const totalContactsResp = 1;
                const usersInCallResp = 90;
                const noContacts = true;
                const contacts = [ { 'name': 'No se pueden desplegar tus contactos',
                    'jid': '',
                    'subscription': 'both',
                    'avatar': '0',
                    'orientation': 1,
                    'status': 'Intenta nuevamente mas tarde' } ];


                this.setState((prevState, nextProps) => {
                    return {
                        data:
                            prevState.page === 1
                                ? Array.from(contacts)
                                : [ ...this.state.data, ...contacts ],
                        totalContacts: totalContactsResp,
                        loading: false,
                        usersInCall: usersInCallResp,
                        noContacts
                    };
                });

            })
            .then(response => {
                let contacts = response.result.users;
                let totalContactsResp = response.result.total;
                let usersInCallResp = response.result.usersincall;
                let noContacts = false;

                if (totalContactsResp === 0 || typeof totalContactsResp === 'undefined') {
                    totalContactsResp = 1;
                    usersInCallResp = 90;
                    noContacts = true;
                    contacts = [ { 'name': 'No tienes ningún contacto',
                        'jid': '',
                        'subscription': 'both',
                        'avatar': '0',
                        'orientation': 1,
                        'status': '' } ];
                }

                this.setState((prevState, nextProps) => {
                    return {
                        data:
                            prevState.page === 1
                                ? Array.from(contacts)
                                : [ ...this.state.data, ...contacts ],
                        totalContacts: totalContactsResp,
                        loading: false,
                        usersInCall: usersInCallResp,
                        noContacts
                    };
                });
            });
        this.searchInprogress = false;
    }

    _handleSearch = search => {
        this.setState(
            (prevState, nextProps) => {
                return {
                    page: 1,
                    search,
                    loading: true,
                    data: []
                };
            },
            () => {
                this._queryContactsRequest(search)
                    .catch(error => {
                        const totalContactsResp = 1;
                        const usersInCallResp = 90;
                        const noContacts = true;
                        const contacts = [ { 'name': 'No se pueden desplegar tus contactos',
                            'jid': '',
                            'subscription': 'both',
                            'avatar': '0',
                            'orientation': 1,
                            'status': 'Intenta nuevamente mas tarde' } ];


                        this.setState((prevState, nextProps) => {
                            return {
                                data:
                                    prevState.page === 1
                                        ? Array.from(contacts)
                                        : [ ...this.state.data, ...contacts ],
                                totalContacts: totalContactsResp,
                                loading: false,
                                usersInCall: usersInCallResp,
                                noContacts
                            };
                        });

                    })
                    .then(response => {
                        let contacts = response.result.users;
                        let totalContactsResp = response.result.total;
                        let usersInCallResp = response.result.usersincall;

                        if (totalContactsResp === 0 || typeof totalContactsResp === 'undefined') {
                            totalContactsResp = 1;
                            usersInCallResp = 90;
                            contacts = [ { 'name': 'No tienes ningún contacto',
                                'jid': '',
                                'subscription': 'both',
                                'avatar': '0',
                                'orientation': 1,
                                'status': '' } ];
                        }

                        this.setState((prevState, nextProps) => {
                            return {
                                data: Array.from(contacts),
                                totalContacts: totalContactsResp,
                                usersInCall: usersInCallResp,
                                loading: false
                            };
                        });
                    });
            }
        );
    }

    _handleLoadMore = () => {
        if (this.state.search === '') {
            if (!this.state.loading && this.state.data.length !== this.state.totalContacts) {
                this.setState(
                    (prevState, nextProps) => {
                        return {
                            page: prevState.page + 1,
                            loading: true
                        };
                    },
                    () => {
                        this._fetchMoreContacts()
                            .catch(error => {
                                const totalContactsResp = 1;
                                const usersInCallResp = 90;
                                const noContacts = true;
                                const contacts = [ { 'name': 'No se pueden desplegar tus contactos',
                                    'jid': '',
                                    'subscription': 'both',
                                    'avatar': '0',
                                    'orientation': 1,
                                    'status': 'Intenta nuevamente mas tarde' } ];


                                this.setState((prevState, nextProps) => {
                                    return {
                                        data:
                                            prevState.page === 1
                                                ? Array.from(contacts)
                                                : [ ...this.state.data, ...contacts ],
                                        totalContacts: totalContactsResp,
                                        loading: false,
                                        usersInCall: usersInCallResp,
                                        noContacts
                                    };
                                });

                            })
                            .then(response => {
                                const contacts = response.result.users;
                                let totalContactsResp = response.result.total;
                                let usersInCallResp = response.result.usersincall;

                                if (totalContactsResp === 0 || typeof totalContactsResp === 'undefined') {
                                    totalContactsResp = 1;
                                    usersInCallResp = 90;
                                    this.state.data = [ { 'name': 'No tienes ningún contacto',
                                        'jid': '',
                                        'subscription': 'both',
                                        'avatar': '0',
                                        'orientation': 1,
                                        'status': '' } ];
                                }

                                this.setState((prevState, nextProps) => {
                                    return {
                                        data:
                                            prevState.page === 1
                                                ? Array.from(contacts)
                                                : [ ...this.state.data, ...contacts ],
                                        totalContacts: totalContactsResp,
                                        usersInCall: usersInCallResp,
                                        loading: false
                                    };
                                });
                            });
                    }
                );
            }
        } else if (!this.state.loading && this.state.data.length !== this.state.totalContacts) {
            this.setState(
                (prevState, nextProps) => {
                    return {
                        page: prevState.page + 1,
                        loading: true
                    };
                },
                () => {
                    this._queryContactsRequest(this.state.search)
                        .catch(error => {
                            const totalContactsResp = 1;
                            const usersInCallResp = 90;
                            const noContacts = true;
                            const contacts = [ { 'name': 'No se pueden desplegar tus contactos',
                                'jid': '',
                                'subscription': 'both',
                                'avatar': '0',
                                'orientation': 1,
                                'status': 'Intenta nuevamente mas tarde' } ];


                            this.setState((prevState, nextProps) => {
                                return {
                                    data:
                                        prevState.page === 1
                                            ? Array.from(contacts)
                                            : [ ...this.state.data, ...contacts ],
                                    totalContacts: totalContactsResp,
                                    loading: false,
                                    usersInCall: usersInCallResp,
                                    noContacts
                                };
                            });

                        })
                        .then(response => {
                            const contacts = response.result.users;
                            let totalContactsResp = response.result.total;
                            let usersInCallResp = response.result.usersincall;

                            if (totalContactsResp === 0 || typeof totalContactsResp === 'undefined') {
                                totalContactsResp = 0;
                                usersInCallResp = 90;
                                this.state.data = [ { 'name': 'No tienes ningún contacto',
                                    'jid': '',
                                    'subscription': 'both',
                                    'avatar': '0',
                                    'orientation': 1,
                                    'status': '' } ];
                            }

                            this.setState((prevState, nextProps) => {
                                return {
                                    data:
                                        prevState.page === 1
                                            ? Array.from(contacts)
                                            : [ ...this.state.data, ...contacts ],
                                    totalContacts: totalContactsResp,
                                    usersInCall: usersInCallResp,
                                    loading: false
                                };
                            });
                        });
                }
            );
        } else if (this.state.totalContacts === 0) {
            this.setState((prevState, nextProps) => {
                return {
                    loading: false
                };
            });
        }
    }

    async _queryContactsRequest(search) {
        const participant = this.props._participant;

        const page = this.state.page;
        const serviceUrl = `${participant.serviceURL}`;
        const idCall = participant.idCall;
        const user = participant.user;
        const jid = participant.jid;
        const action = 'searchusercaninvite';
        const url = `${serviceUrl}?action=${action}&idCall=${idCall}&user=${user}&jid=${jid}&page=${page}&search=${search}`;

        const response = await fetch(url);
        const json = await response.json();

        // console.log(json, participant);

        return json;
    }

    async _fetchMoreContacts() {
        const participant = this.props._participant;

        const page = this.state.page;
        const serviceUrl = `${participant.serviceURL}`;
        const idCall = participant.idCall;
        const user = participant.user;
        const jid = participant.jid;
        const action = 'getuserscaninvitecall';
        const url = `${serviceUrl}?action=${action}&idCall=${idCall}&user=${user}&jid=${jid}&page=${page}`;

        // const url = 'https://im.sociallabs.biz/imweb/APIv1.0.1904051.php?action=testjitsi'
        const response = await fetch(url);
        const json = await response.json();

        // console.log(json, participant);

        return json;
    }

    _renderFooter = () => {
        if (!this.state.loading || this.state.noContacts) {
            return null;
        }

        return (
            <View
                style = {{
                    position: 'relative',
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    marginTop: 10
                }} >
                <ActivityIndicator
                    animating = { true }
                    size = 'large' />
            </View>
        );
    };

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const { _styles } = this.props;


        return [
            <Modal
                key = 'modal'
                onRequestClose = { this._onCancel }
                visible = { true } >
                <Header style = { _styles.headerStyle } >
                    <BackButton onPress = { this._onCancel } />
                    <HeaderLabel labelKey = 'inviteDialog.header' />
                </Header>
                <SafeAreaView style = { _styles.contactListViewStyle } >
                    <View
                        style = { _styles.searchFieldWrapper }>
                        <View style = { _styles.searchIconWrapper }>
                            { this.state.searchInprogress
                                ? <ActivityIndicator
                                    color = { DARK_GREY }
                                    size = 'small' />
                                : <Icon
                                    src = { IconSearch }
                                    style = { _styles.searchIcon } />}
                        </View>
                        <TextInput
                            autoCorrect = { false }
                            editable = { !this.searchInprogress }
                            onChangeText = { this._handleSearch }
                            placeholder = {
                                'Buscar contactos...'
                            }
                            style = { _styles.searchField } />
                    </View>
                    <FlatList
                        data = { this.state.data }
                        initialNumToRender = { 2 }
                        keyExtractor = { (item, index) => index.toString() }
                        ListFooterComponent = { this._renderFooter() }
                        onEndReached = { () => {
                            this._handleLoadMore();
                        } }
                        onEndReachedThreshold = { 0.5 }
                        renderItem = { item => {
                            const buttonProps = {
                                afterClick: this._onCancel,
                                showLabel: true,
                                styles: this.props._styles,
                                userName: item.item.name,
                                contactIsAvailable: item.item.status,
                                jid: item.item.jid,
                                hasAvatar: item.item.avatar,
                                participant: this.props._participant,
                                inCall: false,
                                message: false
                            };

                            if (item.item.name === 'No tienes ningún contacto'
                                || item.item.name === 'No se pueden desplegar tus contactos') {
                                buttonProps.message = true;
                            }

                            if (this.state.usersInCall >= 10) {
                                buttonProps.inCall = true;
                            }

                            return (
                                <InviteButton
                                    key = { item.jid }
                                    { ...buttonProps } />
                            );
                        } }
                        style = { _styles.contactListStyle } />
                </SafeAreaView>
            </Modal>
        ];
    }

    /**
     * Cancels the dialog by calling the onCancel prop callback.
     *
     * @private
     * @returns {void}
     */
    _onCancel() {
        const { onCancel } = this.props;

        onCancel && onCancel();
    }
}

/**
 * Maps part of the Redux state to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @returns {{
 *     _styles: StyleType
 * }}
 */
function _mapStateToProps(state) {
    return {
        _styles: ColorSchemeRegistry.get(state, 'BottomSheet'),
        _participant: getLocalParticipant(state)
    };
}


export default connect(_mapStateToProps)(BottomSheet);
