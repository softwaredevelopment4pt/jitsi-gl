// @flow

import React from 'react';
import { Text, TouchableHighlight, View,
    Image, StyleSheet } from 'react-native';

import { Icon } from '../../icons';

import AbstractToolboxItem from './AbstractToolboxItem';
import type { Props } from './AbstractToolboxItem';

const grey = '#DDDDDD';

const otherStyles = StyleSheet.create({
    subtitle: {
        marginLeft: 10,
        paddingRight: 50,
        color: grey,
        fontSize: 9
    },
    image: {
        width: 44,
        height: 44,
        borderWidth: 1,
        borderRadius: 22
    },
    centrado: {
        textAlign: 'center',
        textAlignVertical: 'center',
        color: grey,
        fontSize: 15
    }
});

const _DEFAULT_SOURCE = require('../../../../../images/profile.png');


/**
 * Native implementation of {@code AbstractToolboxItem}.
 */
export default class ToolboxItem extends AbstractToolboxItem<Props> {
    /**
     * Transform the given (web) icon name into a name that works with
     * {@code Icon}.
     *
     * @private
     * @returns {string}
     */
    _getIconName() {
        const { iconName } = this.props;

        return iconName.replace('icon-', '').split(' ')[0];
    }

    /**
     * Renders the {@code Icon} part of this {@code ToolboxItem}.
     *
     * @private
     * @returns {ReactElement}
     */
    _renderIcon() {
        const { styles } = this.props;

        return (
            <Icon
                src = { this.props.icon }
                style = { styles && styles.iconStyle } />
        );
    }


    /**
     * Renders the {@code Icon} part of this {@code ToolboxItem}.
     *
     * @private
     * @returns {ReactElement}
     */
    _renderImage() {
        const { styles } = this.props;

        return (
            <Image
                source = {{ uri: 'https://facebook.github.io/" +'
                        + '/react-native/docs/assets/favicon.png' }}
                style = { styles && styles.iconStyle } />
        );
    }


    /**
     * Renders this {@code ToolboxItem}. Invoked by {@link AbstractToolboxItem}.
     *
     * @override
     * @protected
     * @returns {ReactElement}
     */
    _renderItem() {
        const {
            disabled,
            elementAfter,
            onClick,
            showLabel,
            styles,
            contactIsAvailable,
            hasAvatar,
            jid,
            participant,
            message
        } = this.props;

        let children = this._renderIcon();
        let urlImagen = '';

        if (hasAvatar === '1') {
            urlImagen = `${participant.imageURL}/${jid}.png`;
        }

        // XXX When using a wrapper View, apply the style to it instead of
        // applying it to the TouchableHighlight.
        let style = styles && styles.style;

        const elLabel = this.label ? this.label : '';
        const labels = elLabel.split('main:');

        if (showLabel) {
            // XXX TouchableHighlight requires 1 child. If there's a need to
            // show both the icon and the label, then these two need to be
            // wrapped in a View.

            const date = new Date().getTime();

            urlImagen = `${urlImagen}?stamp=${date}`;

            if (hasAvatar === '1' && message === false) {

                children = (
                    <View style = { style }>
                        <Image
                            source = {{ uri: urlImagen }}
                            style = { otherStyles.image } />
                        <View>
                            <Text
                                ellipsizeMode = {'tail'}
                                numberOfLines = { 1 }
                                style = { styles.labelStyle }>
                                {labels}</Text>

                            <Text
                                ellipsizeMode = {'tail'}
                                numberOfLines = { 1 }
                                style = { otherStyles.subtitle }>
                                {contactIsAvailable}</Text>
                        </View>
                        { elementAfter }
                    </View>
                );
            } else if (hasAvatar === '0' && message === false)  {

                children = (
                    <View style = { style }>
                        <Image
                            source = { _DEFAULT_SOURCE }
                            style = { otherStyles.image } />
                        <View>
                            <Text
                                ellipsizeMode = {'tail'}
                                numberOfLines = { 1 }
                                style = { styles.labelStyle }>
                                {labels}</Text>

                            <Text
                                ellipsizeMode = {'tail'}
                                numberOfLines = { 1 }
                                style = { otherStyles.subtitle }>
                                {contactIsAvailable}</Text>
                        </View>
                        { elementAfter }
                    </View>
                );

            } else {
                children = (
                    <View style = { otherStyles.centrado }>
                        <View>
                            <Text
                                ellipsizeMode = {'tail'}
                                numberOfLines = { 1 }
                                style = { otherStyles.centrado }>
                                {labels}</Text>

                            <Text
                                ellipsizeMode = {'tail'}
                                numberOfLines = { 1 }
                                style = { otherStyles.centrado }>
                                {contactIsAvailable}</Text>
                        </View>
                        { elementAfter }
                    </View>
                );
            }

            // XXX As stated earlier, the style was applied to the wrapper View
            // (above).
            style = undefined;
        }

        return (
            <TouchableHighlight
                accessibilityLabel = { this.accessibilityLabel }
                disabled = { disabled }
                onPress = { onClick }
                style = { style }
                underlayColor = { styles && styles.underlayColor } >
                { children }
            </TouchableHighlight>
        );
    }
}
