// @flow

import type { Dispatch } from 'redux';

import { translate } from '../../../../base/i18n';
import { connect } from '../../../../base/redux';
import { AbstractButton } from '../../../../base/toolbox';
import type { AbstractButtonProps } from '../../../../base/toolbox';

import { isAddPeopleEnabled, isDialOutEnabled } from '../../../functions';
import { getLocalParticipant } from '../../../../base/participants';


type Props = AbstractButtonProps & {

    /**
     * The Redux dispatch function.
     */
    dispatch: Dispatch<any>,

    userName: string,

    contactIsAvailable: string,
    hasAvatar: string,
    jid: string,
    participant: any
};

/**
 * Implements an {@link AbstractButton} to enter add/invite people to the
 * current call/conference/meeting.
 */
class InviteButton extends AbstractButton<Props, *> {
    accessibilityLabel = 'toolbar.accessibilityLabel.shareRoom';
    iconName = 'icon-link';
    label = 'toolbar.shareRoom';


    /**
     * Handles clicking / pressing the button, and opens the appropriate dialog.
     * Here must be handled the label.
     *
     * @param {Object} props - The redux store/state.
     * @returns {void}
     */
    constructor(props: Props) {
        super(props);

        super.label = props.userName;
        super.contactIsAvailable = props.contactIsAvailable;
        super.hasAvatar = props.hasAvatar;
        super.jid = props.jid;


        this.elParticipant = props.participant;
    }


    /**
     * Handles clicking / pressing the button, and opens the appropriate dialog.
     *
     * @private
     * @returns {void}
     */
    _handleClick() {
        _requestInvitation(this.elParticipant, this.props.jid).then(object => {
            console.log('Invite Response: ', object);
        });
    }

    /**
     * Returns true if none of the invite methods are available.
     *
     * @protected
     * @returns {boolean}
     */
    _isDisabled() {
        return this.props.inCall;
    }
}

/**
 * Maps (parts of) the redux state to {@link InviteButton}'s React {@code Component}
 * props.
 *
 * @param {Object} state - The redux store/state.
 * @private
 * @returns {{
 *   _addPeopleEnabled: boolean
 * }}
 */
function _mapStateToProps(state) {
    return {
        _addPeopleEnabled: isAddPeopleEnabled(state) || isDialOutEnabled(state),

        /**
         * Whether or not the feature to dial out to number to join the
         * conference is available.
         *
         * @type {boolean}
         */
        _participant: getLocalParticipant(state)
    };
}

/**
 * Request to invite contacts.
 *
 *
 * @param {Object} participantlocal -  To convert.
 * @param {Object} bid - The JWT {@code context.user} structure to convert.
 *
 * @returns {{
 *     avatarURL: ?string,
 *     email: ?string,
 *     id: ?string,
 *     name: ?string
 * }}
 */
async function _requestInvitation(participantlocal, bid) {
    const url = `${participantlocal.serviceURL}`;

    const form = new FormData();

    form.append('action', 'sendincomingcall');
    form.append('idCall', participantlocal.idCall);
    form.append('jid', participantlocal.jid);
    form.append('bid', bid);
    form.append('type', participantlocal.type);

    const json = await fetch(url, { method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        body: form
    });

    return json;
}

export default translate(connect(_mapStateToProps)(InviteButton));

